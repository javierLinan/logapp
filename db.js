module.exports = function(object, config, lang) {

  switch(config.dbSystem) {
    case 'mongodb':
      object.connect('mongodb://' + config.server + '/' + config.dbName, function(err) {
        if(err) { throw err;} 
      });
      break;
    default:
      throw new Error(lang.error.db.dbSystemNotSpecified);
  }
};