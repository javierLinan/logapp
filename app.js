var express       = require('express'),
  path            = require('path'),
  bodyParser      = require('body-parser'),
  session         = require('express-session'),
  MongoStore      = require('connect-mongo')(session),
  cookieParser    = require('cookie-parser'),
  flash           = require('connect-flash'),
  mongoose        = require('mongoose'),
  passport        = require('passport'),
  app             = express(),
  config,
  routes,
  lang;

// Configuration variables
config = require('./config/vars');

// Lang strings
lang = require('./lang/' + config.lang.default + '/index');

// Database connection
require('./db')(mongoose, config.db.mongodb, lang);

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static(path.join(__dirname, config.dir.static)));
app.use(cookieParser('keyboard cat'));
app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: config.session.secret,
  cookie: { maxAge: 60000 },
  store: new MongoStore({
    db : mongoose.connection.db
  })
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// Database schemas registration
require('./models/user')(mongoose);

// Passport configuration
require('./config/passport')(passport, mongoose, config.passport, lang);

// View engine setup
app.set('views', path.join(__dirname, config.dir.views));
app.set('view engine', config.engines.views);

// Routes
routes = require('./routes/index');
app.use('/', routes);

// Session-persisted message middleware
app.use(function(req, res, next){
  var err = req.session.error;
  var msg = req.session.success;
  delete req.session.error;
  delete req.session.success;
  res.locals.message = '';
  if (err) res.locals.message = '<p class="msg error">' + err + '</p>';
  if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';
  next();
});

/// Catch 404 and forward to error handler()
app.use(function(req, res, next) {
  var err = new Error(lang.error.generic.notFound);
  err.status = 404;
  next(err);
});

/// Error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', { 
    message: err.message,
    error: {}
  });
});

module.exports = app;
