var mongoose    = require('mongoose'),
  User          = mongoose.model('User'),
  bcrypt        = require('bcryptjs'),
  config        = require('../config/vars'),
  lang          = require('../lang/' + config.lang.default + '/index');

module.exports.register = function(req, res) {

  User.findOne({ email: req.body.email }, function(err, user) {

    if (user) {

      req.flash('error', lang.error.register.duplicated);
      res.redirect('/register');

    } else {
      
      bcrypt.hash(req.body.passwd, 8, function(err, hash) {

        var newUser = new User({ 
          email:    req.body.email,
          name:     req.body.name,
          surname:  req.body.surname,
          passwd:   hash
        });

        newUser.save(function(err, newUser) {
          if(err) {  
            throw err.message;
          }

          req.flash('success', lang.success.registered);
          res.redirect('/login');
        });  
      });     
    }
  });     
};

module.exports.logout = function(req, res) {
  req.logout();
  req.flash('success', lang.success.logout);
  res.redirect('/');
};