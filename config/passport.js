module.exports = function(passport, mongoose, config, lang) {
  var User          = mongoose.model('User'),
    LocalStrategy   = require('passport-local').Strategy,
    bcrypt          = require('bcryptjs');


  passport.use(new LocalStrategy({
      usernameField: config.usernameField,
      passwordField: config.passwordField
    },    
    function(username, password, done) {

      User.findOne({ email: username }, function(err, user) {

        if (err) { 
          return done(err); 
        }
        
        if (!user) {
          return done(null, false, { message: lang.error.auth.badUsername });
        }

        bcrypt.compare(password, user.passwd, function(err, res) {
          if (!res) {
            return done(null, false, { message: lang.error.auth.badPassword });
          }
            
          return done(null, user);
        });
      });
    })
  );

  passport.serializeUser(function(user, done) {
     done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {

    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};

