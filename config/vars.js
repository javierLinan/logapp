module.exports = {
  db: {
    mongodb: {
      dbSystem:       'mongodb',
      server:         'localhost',
      port:           '',
      username:       '',
      password:       '',
      dbName:         'applog'
    }
  },
  dir: {
    static:           'public',
    views:            'views'
  },
  session: {
    secret:           'smellycat'
  },
  passport: {
    usernameField:    'username',
    passwordField:    'passwd'
  },
  lang: {
    default:          'en'
  },
  engines: {
    views:            'jade'
  },
  cdn: {
    css: {
      0:              '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css',
      1:              '//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css'
    },
    js: {
      0:              '//code.jquery.com/jquery-2.1.1.min.js',
      1:              '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
      2:              '//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js'
    }
  }
};