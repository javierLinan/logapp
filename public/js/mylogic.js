$(document).ready(function() {
  $('#login-form').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      username: {
        validators: {
          notEmpty: {
            message: 'The username is required and cannot be empty'
          }
        }
      },
      passwd: {
        validators: {
          notEmpty: {
            message: 'The password is required and cannot be empty'
          }
        }
      }
    }
  });

  $('#register-form').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      name: {
        validators: {
          notEmpty: {
            message: 'The name is required and cannot be empty'
          }
        }
      },
      surname: {
        validators: {
          notEmpty: {
            message: 'The surname is required and cannot be empty'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'The email is required and cannot be empty'
          },
          emailAddress: {
            message: 'The input is not a valid email address'
          }
        }
      },
      passwd: {
        validators: {
          notEmpty: {
            message: 'The password is required and cannot be empty'
          },
          identical: {
            field: 'rpasswd',
            message: 'The password and its confirm are not the same'
          }                    
        }
      },
      rpasswd: {
        validators: {
          notEmpty: {
            message: 'The password is required and cannot be empty'
          },          
          identical: {
            field: 'passwd',
            message: 'The password and its confirm are not the same'
          }                     
        }
      }
    }          
  });
});