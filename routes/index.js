var router        = require('express').Router(),
  passport        = require('passport'),
  config          = require('../config/vars'),
  userController  = require('../controllers/user'),
  config          = require('../config/vars'),
  lang            = require('../lang/' + config.lang.default + '/index')

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { 
    title:    'Home',
    error:    req.flash('error'),
    success:  req.flash('success'),
    css:      config.cdn.css,
    js:       config.cdn.js,
    user:     req.user
  });
});

/* GET new user page. */
router.get('/register', function(req, res) {

  if(req.user) {
    res.redirect('/');
  } 

  res.render('user-register', { 
    title:    'Create user',
    error:    req.flash('error'),
    success:  req.flash('success'),
    css:      config.cdn.css,
    js:       config.cdn.js,
    user:     req.user
  });      
});

/* GET login page. */
router.get('/login', function(req, res) {
  if(req.user) {
    res.redirect('/');
  } 

  res.render('user-login', { 
    title:    'Login',
    error:    req.flash('error'),
    success:  req.flash('success'),
    css:      config.cdn.css,
    js:       config.cdn.js,
    user:     req.user
  });   
});

/* POST new user page. */
router.post('/register', userController.register);

/* POST login page. */
router.post('/login', 
  passport.authenticate('local', { 
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
    successFlash: lang.success.logged
  })
);

/* GET logout page. */
router.get('/logout', userController.logout);

module.exports = router;
