module.exports = {
  error: {    
    db: {
      dbSystemNotSpecified:   'Database management system unspecified.'
    },
    auth: {
      badUsername:            'Incorrect username.',
      badPassword:            'Incorrect password.'
    },
    generic: {
        notFound:             'Not found.'
    },
    register: {
      duplicated:             'Already exists an user with the same email.'
    }
  },
  success: {
    logged:                   'Welcome to the site!',
    logout:                   'See you!',
    registered:               'Your account was created successfully'
  }
}