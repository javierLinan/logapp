module.exports = function(mongoose) {

  var userSchema = new mongoose.Schema({
    email:      { type: String, required: true, index: { unique: true } },
    name:       { type: String, required: true },
    surname:    { type: String, required: true },
    passwd:     { type: String, required: true }
  });

  mongoose.model('User', userSchema);
};