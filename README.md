# logapp #

This is a simple node.js / express application that allow you to add users in a mongo database using a register form and registration. The session is persistent and the user password is stored codified.

## How do I get set up? ##

Install Node.js and MongoDB, then:

```
#!

$ git clone https://javierLinan@bitbucket.org/javierLinan/logapp.git
$ cd logapp
$ sudo npm install
$ node ./bin/www
$ Access to http://localhost:3000
```